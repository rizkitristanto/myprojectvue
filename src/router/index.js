import Vue from "vue";
import VueRouter from "vue-router";
import Clean from "@/views/layouts/CleanLayout.vue";

Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Root",
      component: Clean,
      redirect: "login",
      children: [
        {
          path: "login",
          name: "login",
          component: () => import("@/views/pages/login")
        }
      ]
    },
    {
      path: "*",
      name: "Not Found",
      component: () => import("@/views/Error/NotFound.vue")
    }
  ]
});
export default router;
